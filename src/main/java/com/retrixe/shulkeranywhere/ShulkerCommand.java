package com.retrixe.shulkeranywhere;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.ShulkerBox;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;

public class ShulkerCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender.equals(sender.getServer().getConsoleSender())) {
            sender.sendMessage(ChatColor.RED + "Only players can use this command!");
            return true;
        }
        Player player = (Player)sender;
        if (!ShulkerListener.isShulkerBox(player.getInventory().getItemInMainHand())) {
            sender.sendMessage(ChatColor.RED + "You are not holding a shulker box!");
            return true;
        }
        try {
            // Open up a GUI where it can be accessed.
            ItemStack shulkerBoxItem = player.getInventory().getItemInMainHand();
            BlockStateMeta shulkerBoxMeta = (BlockStateMeta)shulkerBoxItem.getItemMeta();
            if (shulkerBoxMeta != null) {
                ShulkerBox shulkerBox = (ShulkerBox) shulkerBoxMeta.getBlockState();
                Inventory shulkerInventory = Bukkit.createInventory(
                        null, InventoryType.SHULKER_BOX, "Shulker Box (/shulker)");
                shulkerInventory.setContents(shulkerBox.getInventory().getContents());
                player.openInventory(shulkerInventory);
                ShulkerListener.shulkerBoxMap.put(player.getUniqueId(), shulkerBoxItem);
            }
        } catch (Exception e) {
            sender.sendMessage(ChatColor.RED + "An unknown error occurred.");
        }
        return true;
    }
}

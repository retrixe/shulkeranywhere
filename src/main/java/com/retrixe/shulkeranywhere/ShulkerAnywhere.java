package com.retrixe.shulkeranywhere;

import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;

public class ShulkerAnywhere extends JavaPlugin {
    @Override
    public void onEnable () {
        try {
            PluginCommand shulker = this.getCommand("shulker");
            if (shulker != null) shulker.setExecutor(new ShulkerCommand());
            this.getServer().getPluginManager().registerEvents(new ShulkerListener(), this);
        } catch (Exception e) {
            this.getLogger().log(Level.SEVERE, "Failed to register command/listener!", e);
        }
    }
}

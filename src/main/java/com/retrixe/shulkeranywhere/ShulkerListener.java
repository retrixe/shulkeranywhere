package com.retrixe.shulkeranywhere;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.ShulkerBox;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;

import java.util.HashMap;
import java.util.UUID;

public class ShulkerListener implements Listener {
    public static HashMap<UUID, ItemStack> shulkerBoxMap = new HashMap<>();

    // Open the shulker box inventory when doing Shift+Right Click with one in the hand.
    @EventHandler
    public void onPlayerInteractEvent(PlayerInteractEvent e) {
        ItemStack mainHand = e.getPlayer().getInventory().getItemInMainHand();
        if (e.getPlayer().hasPermission("ShulkerAnywhere.shulker") &&
                e.getAction().equals(Action.RIGHT_CLICK_AIR) &&
                e.getPlayer().isSneaking() &&
                isShulkerBox(mainHand)
        ) {
            try {
                // Open up a GUI where it can be accessed.
                ItemStack shulkerBoxItem = e.getPlayer().getInventory().getItemInMainHand();
                BlockStateMeta shulkerBoxMeta = (BlockStateMeta) shulkerBoxItem.getItemMeta();
                if (shulkerBoxMeta != null) {
                    ShulkerBox shulkerBox = (ShulkerBox) shulkerBoxMeta.getBlockState();
                    Inventory shulkerInventory = Bukkit.createInventory(
                            null, InventoryType.SHULKER_BOX, "Shulker Box (/shulker)");
                    shulkerInventory.setContents(shulkerBox.getInventory().getContents());
                    e.getPlayer().openInventory(shulkerInventory);
                    shulkerBoxMap.put(e.getPlayer().getUniqueId(), shulkerBoxItem);
                }
            } catch (Exception err) {
                e.getPlayer().sendMessage(ChatColor.RED + "An unknown error occurred.");
            }
        }
    }

    // Save the inventory back into the shulker box.
    @EventHandler
    public void onInventoryCloseEvent(InventoryCloseEvent e) {
        boolean isShulkerCommand = e.getView().getTitle().equals("Shulker Box (/shulker)");
        // This shouldn't happen, really.
        if (isShulkerCommand) {
            ItemStack shulkerBoxItem = shulkerBoxMap.remove(e.getPlayer().getUniqueId());
            if (shulkerBoxItem == null) {
                e.getPlayer().sendMessage(ChatColor.RED + "An unknown error occurred.");
                return;
            }
            try {
                // Read the current inventory.
                BlockStateMeta shulkerBoxMeta = (BlockStateMeta) shulkerBoxItem.getItemMeta();
                assert shulkerBoxMeta != null; // cba to do anything better than this
                ShulkerBox shulkerBox = (ShulkerBox) shulkerBoxMeta.getBlockState();
                // Save the contents.
                shulkerBox.getInventory().setContents(e.getInventory().getContents());
                shulkerBoxMeta.setBlockState(shulkerBox);
                shulkerBoxItem.setItemMeta(shulkerBoxMeta);
            } catch (Exception err) {
                err.printStackTrace();
                e.getPlayer().sendMessage(ChatColor.RED + "An unknown error occurred.");
            }
        }
    }

    @EventHandler
    public void onInventoryClickEvent(InventoryClickEvent e) {
        // Check whether a shulker box inventory is open.
        if (!e.getView().getTitle().equals("Shulker Box (/shulker)") || e.getCursor() == null) return;
        boolean isShulkerInv = e.getView().getTopInventory().equals(e.getClickedInventory());

        // We check if the item in hand is being moved, via click or hot bar. Drag or shift click won't bypass this.
        if (e.getSlot() == e.getWhoClicked().getInventory().getHeldItemSlot() &&
                !isShulkerInv &&
                isShulkerBox(e.getWhoClicked().getInventory().getItemInMainHand())
        ) e.setCancelled(true);
        else if (
            !isShulkerInv
            && e.getAction().name().contains("HOTBAR")
            && e.getHotbarButton() == e.getWhoClicked().getInventory().getHeldItemSlot()
            && isShulkerBox(e.getWhoClicked().getInventory().getItemInMainHand())
        ) e.setCancelled(true);
    }

    public static boolean isShulkerBox(ItemStack itemStack) {
        return itemStack != null &&
                itemStack.getItemMeta() instanceof BlockStateMeta &&
                ((BlockStateMeta) itemStack.getItemMeta()).getBlockState() instanceof ShulkerBox;
    }
}

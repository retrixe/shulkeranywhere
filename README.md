# ShulkerAnywhere

Access shulker boxes in servers from anywhere.

## Usage

Players need the `ShulkerAnywhere.shulker` permission, granted to OP by default.

Players with this permission can hold a shulker box in their main hand and run /shulker to access its contents. They can also Shift+Right Click air (crouch and right click in air) with the shulker box in hand to open it.

## [Download](https://gitlab.com/retrixe/shulkeranywhere/-/releases)

This plugin supports Minecraft 1.13+. For 1.12.2, use version 1.0.0 and earlier.

## Build

`./gradlew shadowJar` (`.\gradlew.bat` on Windows) will generate a JAR file for the plugin. You can also use IntelliJ to run the Gradle task.

The JAR will be stored at build/libs.
